# Sudoku

## You need elixir on your system
 - http://elixir-lang.org/install.html#unix-and-unix-like  
If everything is ok you should see something like this:
```
➜  elixir git:(f045705) elixir --version
Erlang/OTP 19 [erts-8.2] [source-fbd2db2] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false]

Elixir 1.4.0
➜  elixir git:(f045705) iex --version
Erlang/OTP 19 [erts-8.2] [source-fbd2db2] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false]

IEx 1.4.0
```

- try to load the app with  
```
iex -S mix
```

## Release process
```
# update mix.exs to bump the release version
mix deps.get
MIX_ENV=prod mix compile --no-debug-info
MIX_ENV=prod mix release
git push # the release ~ 12mo
# update the server with the hosts-provisionning repo
# TODO add the restart command at the end of the deploiement process
rel/sudoku/bin/sudoku start # bug: even after stop the old release is running --> htop kill
```